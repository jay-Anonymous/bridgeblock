<h1 align="center">
  <img src="https://gitlab.com/vander-security/bridgeblock/-/raw/main/GUI/BridgeBlock/Assets.xcassets/AppIcon.appiconset/icon_512.png?inline=false"
  align="center" title="App logo by IconKitchen" width="105" height="105" />

  <br/>
  BridgeBlock  <br/>
</h1>

<p align="center">
  <em><strong>BridgeBlock</strong> is a menu bar app that prevents wireless bridging attacks by detecting bridging conditions and taking action.<br>
    It also has a commandline version for system administrators.</em>
</p>

 <img src="https://gitlab.com/vander-security/bridgeblock/-/raw/main/Screenshot_2023-01-11_at_14.39.12.png?inline=false"
  align="center" title="Screenshot of GUI" width="250" height="150" />

  <br>
  <strong>Sponsors:</strong>
  <br/><a href="https://vandersecurity.com/tools"><img src="https://img.shields.io/badge/Vander%20Security-💛%20Open%20Source-blue" alt="Vander Security: Open Source Tools" /></a>
</p>




## Installing

<br/><a href="https://vandersecurity.com/tools"><img src="https://img.shields.io/badge/license-GPLv2-green" alt="Licensed under the GPLv2." /></a>
</p>

### GitLab Releases

Zips of the both the GUI and CLI versions are provided in the <a href="https://gitlab.com/vander-security/bridgeblock/-/releases">releases</a> page. You can unzip them and run them from anywhere, but you'll probably want to install the GUI version in `/Applications` and the CLI version in `/usr/local/bin`.

### Homebrew
Homebrew packages will be made availible in the 2.0 release, pending feedback from the community.

### Requirements

macOS, 13.0 (Ventura) or later.


## Known Issues

BridgeBlock works by powering off the AirPort module, so any functionality that relies on it will be temporarily unavailable as well. AirDrop, AirPlay, Apple Watch Unlocking, etc. The kind of environments that need BridgeBlock probably have these disabled anyways.

You cannot use the CLI and GUI versions of BridgeBlock simultainiously. They will conflict with each other and cause erratic behaviour of the AirPort module.

## How BridgeBlock Works

BridgeBlock gets a list of network interfaces on the system, then tests each of the Ethernet interfaces for connectivity by the pinging URL you provided through each interface. If connectivity is present, the AirPort module is powered down. Without WiFi communication, the ability of an attacker to interact with the Mac over WiFi is prevented.

## Contributing

We are grateful for all community contributions. If you see an issue, feel free to file a bug report and submit a patch if you can. Contributors are expected to treat other with respect and dignity. We don't care about your personal life/opinions, but it shouldn't be something that draws negative attention to the project.
