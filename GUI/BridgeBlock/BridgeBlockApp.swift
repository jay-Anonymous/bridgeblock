//
//  BridgeBlockApp.swift
//  BridgeBlock
//
//  Created by Andrew Vanderbilt on 1/7/23.
//

import SwiftUI
import Combine

@main
struct BridgeBlockApp: App {
    var body: some Scene {
        MenuBarExtra("BridgeBlock", systemImage: "network.badge.shield.half.filled") {
            MenuView()
        }.menuBarExtraStyle(.window)
    }
}

class LoopTimer {
    
    // Construct the timer
    private var timer: Timer?
    
    // Path the the Plist we scrape for network data.
    private let networkInterfacesPlist = URL(string: "file:///Library/Preferences/SystemConfiguration/NetworkInterfaces.plist")!
    
    // Path to the temp file we use to preprocess the plist.
    private let fixedPlistPath = URL(string: "file:///var/tmp/fixed_plist.plist")!
    
    // The variables we need to instantiate the Timer with.
    // We have to provide initialization values because Swift.
    var timerURL: String = ""
    var timerTries: Int = 3
    var timerCheckFrequency = 10
    // We default to false because we don't want to leave the user without internet ever.
    var hasInternet = false
    
    
    // Fire off the timer when called.
    func startTimer() {
        DispatchQueue.global(qos: .background).async {
            let timer = Timer.scheduledTimer(withTimeInterval: TimeInterval(self.timerCheckFrequency), repeats: true) { _ in
                self.checkInterfaces()
            }
            let runLoop = RunLoop.current
                    runLoop.add(timer, forMode: .default)
                    runLoop.run()
        }
    }
    
    // Kill the current timer to prevent zombies.
    func stopTimer() {
        
        timer?.invalidate()
        timer = nil
    }
    
    
    // Set up struct to convert Plist values to native objects.
    // https://stackoverflow.com/questions/60803515/swift-5-how-to-read-variables-in-plist-files
    struct Root: Decodable {
        let Interfaces: [Item]
    }
    
    struct SCNetworkInterfaceInfo: Decodable {
        let UserDefinedName: String
        // Not all interfaces will have these attributes.
        let idProduct: Int?
        let idVendor: Int?
        let kUSBProductString: String?
    }
    
    // Weird bug here. The Plist Decodable method can't handle whitepsce in key strings.
    // For some reason, Apple uses a space in the BSD Name key.
    // To fix this, we proprocess the file before it reaches the decoder.
    // https://stackoverflow.com/questions/73884814/no-value-associated-with-key-codingkeysstringvalue-index-intvalue-nil
    struct Item: Decodable {
        
        // It seems that instead of setting this to false, they just won't include in the dict.
        let Active: Bool?
        let BSDName: String
        let IOBuiltin: Bool
        let IOInterfaceNamePrefix: String
        let IOInterfaceType: Int
        let IOInterfaceUnit: Int
        let IOMACAddress: Data
        let IOPathMatch: String
        let SCNetworkInterfaceInfo: SCNetworkInterfaceInfo
        let SCNetworkInterfaceType: String
    }
    
    // Pulls the list of available interfaces, returns nil if none found.
    // We'll handle the return of nil in the code that calls this function.
    func getInterfaces() -> [Item]? {
        
        do {
            
            // Read the copied file as a string.
            let brokenPlistData = try String(contentsOf: networkInterfacesPlist)
            
            // Remove the stupid space from the property key.
            let fixedPlistData = brokenPlistData.replacingOccurrences(of: "BSD Name", with: "BSDName")
            // .replacingOccurrences(of: "my sanity", with: "wtf apple?")
            
            // Write the data into a temporary file.
            try fixedPlistData.write(to: fixedPlistPath, atomically: true, encoding: .utf8)
            
            // Ingest the file using Data.
            let plistData = try Data(contentsOf: fixedPlistPath)
            
            // Decode the contents.
            var interfaces = [Item]()
            let result = try PropertyListDecoder().decode(Root.self, from: plistData)
            
            // Conform to Interfaces type.
            interfaces = result.Interfaces
            
            // Delete temporary file.
            try FileManager.default.removeItem(at: fixedPlistPath)
            
            return(interfaces)
            
        } catch {
            print(error)
        }
        // If we get an empty [Item], return nil.
        // We'll handle the return of nil in the code that calls this function.
        return(nil)
    }
    
    // Currently, we let the system guess which interface is wifi and to turn off.
    // TODO: Make an array of BSD names which are of 80211 interface type, and specifically set their power to off.
    @discardableResult // Make compiler shhhhh.
    func setAirport(state: String) -> Bool {
        
        let task = Process()
        let pipe = Pipe()
        
        task.standardOutput = pipe
        task.standardError = pipe
        
        // See the TODO above.
        // WIFI is not a valid interface name, so the system will assume what we want.
        task.arguments = ["-setairportpower", "WIFI", state]
        task.executableURL = URL(fileURLWithPath: "/usr/sbin/networksetup")
        task.standardInput = nil
        
        do {
            
            try task.run()
            
        } catch {
            
            return(false)
        }
        
        return(true)
    }
    
    func canPing(host: String, interface: String) -> Bool {
        
        let task = Process()
        let pipe = Pipe()
        
        task.standardOutput = pipe
        task.standardError = pipe
        // Time out after 1 second.
        // Ping usually only takes 20-200 ms, so if we are exceeding 1000 something is wrong.
        task.arguments = ["-c", String(timerTries), "-t", "1", "-b", interface, host]
        task.executableURL = URL(fileURLWithPath: "/sbin/ping")
        task.standardInput = nil
        
        do {
            try task.run()
        } catch {
            return false
        }
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output = String(data: data, encoding: .utf8)!
        
        if output.contains("ping: sendto: No route to host") {
            // The interface (probably) does not have a working internet connection.
            return(false)
        } else if output.contains("round-trip min/avg/max/stddev") {
            // This only appears in the results if the ping was successful at least once.
            return(true)
        } else {
            return false
        }
    }
    
    func checkInterfaces() {
        let interfaces = getInterfaces()
        
        for item in interfaces ?? [] {
            
            // Have to handle the possibility of there being no interfaces.
            // Realistically, this should never happen due to the SE coprocessor bridges, but still.
            if interfaces == nil {
                abort()
            }
            
            // We don't want to count iBridges or iOS devices. These interfaces are used for coprocessors or shared hotspots.
            // The interfaces we want must also be listed as Active.
            if item.SCNetworkInterfaceType == "Ethernet" && item.SCNetworkInterfaceInfo.UserDefinedName != "iBridge" &&
                item.SCNetworkInterfaceInfo.UserDefinedName != "iPad" && item.SCNetworkInterfaceInfo.UserDefinedName != "iPhone" && item.Active == true {
                
                // Send out a ping using the interface.
                if canPing(host: timerURL, interface: item.BSDName) {
                    hasInternet = true
                    setAirport(state: "off")
                    
                    // No need to test the rest, we have fufilled our purpose.
                    // If this isn't here we'd rapidly turn the WiFi on and off as we test.
                    break
                } else {
                    hasInternet = false
                }
            }
            
        }
        // The connectivity test failed. Turn Airport back on.
        if hasInternet == false {
            setAirport(state: "on")
        }
    }
    
}



struct MenuView: View {
    
    @State var enabled = false
    @State var interfaceURL : String
    @State var interfaceTries : Int
    @State var interfaceCheckFrequency : Int
    @State var showValidationAlert = false
    @State private var loop = LoopTimer()
    
    init() {
        
        // Get UserDefaults object.
        let defaults = UserDefaults.standard
        interfaceURL = defaults.string(forKey: "interfaceURL") ?? "google.com"
        interfaceTries = defaults.integer(forKey: "interfaceTries")
        interfaceCheckFrequency = defaults.integer(forKey: "interfaceCheckFrequency")
    }
    
    func exitApp() {
        
        // Get UserDefaults object.
        let defaults = UserDefaults.standard
            defaults.setValue(interfaceURL, forKey: "interfaceURL")
            defaults.setValue(interfaceTries, forKey: "interfaceTries")
            defaults.setValue(interfaceCheckFrequency, forKey: "interfaceCheckFrequency")
            defaults.synchronize()
        exit(0)
    }
    
    // This isn't foolproof, but it is good for 90% of what they might put in there.
    func validateURL() {
        
        // Holy regex batman!
        let urlRegEx = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        let result : Bool = urlTest.evaluate(with: interfaceURL)
        
        if result == true {
            // if enabled is true, then invalidate the current timer, start a new one with the new URL.
            if  enabled == true {
                loop.stopTimer()
                loop.setAirport(state: "on")
                loop.startTimer()
            }
        } else {
            showValidationAlert = true
            interfaceURL = "https://google.com"
        }
    }
    
    var body: some View {
        VStack {
            HStack {
                Text("Bridge Protection")
                    .bold()
                    .font(.system(size: 13))
                
                Spacer()
                
                Toggle(isOn: $enabled) {
                    // Start timer
                }
                .toggleStyle(SwitchToggleStyle())
                .onChange(of: enabled) { _ in
                    if enabled == true {
                        
                        // Set Timer vars.
                        loop.timerURL = interfaceURL
                        loop.timerTries = interfaceTries
                        loop.timerCheckFrequency = interfaceCheckFrequency
                        
                        // Start timer
                        loop.startTimer()

                    } else if enabled == false {
                        loop.stopTimer()
                        loop.setAirport(state: "on")

                    }
                }
            }
            
            Divider()
            
            HStack {
                Text("**Test URL**")
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            
            HStack {
                TextField("URL...", text: $interfaceURL)
                Button(action: {
                    validateURL()
                }) {
                    
                    HStack {
                        Text("Set")
                    }
                    
                }
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            
            Divider()
            
            HStack {
                Picker("**Retry Attempts**", selection: $interfaceTries) {
                    ForEach(1...10, id: \.self) { interfaceTries in
                        Text("\(interfaceTries) tries")
                    }
                }
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            
            HStack {
                Picker("**Check Every**", selection: $interfaceCheckFrequency) {
                    ForEach(1...30, id: \.self) { interfaceCheckFrequency in
                        Text("\(interfaceCheckFrequency) seconds")
                    }
                }
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            
            Divider()
            
            HStack {
                
                Button(action: {
                    exitApp()
                }) {
                    
                    HStack {
                        
                        Text("Quit...")
                            .frame(maxWidth: .infinity, minHeight: 25, alignment: .leading)
                        
                    }
                    .contentShape(Rectangle())
                    
                }
                .buttonStyle(PlainButtonStyle())
                
            }
            .padding(.bottom, -10)
            .padding(.top, -5)
            
        }
        .alert("The specified URL is invalid.", isPresented: $showValidationAlert) {
            Button("OK", role: .cancel) { }
        }
        .frame(maxWidth: 250)
        .padding()
        .padding(.top, -5)
    }
    
}
