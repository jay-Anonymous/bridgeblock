//
//  BridgeBlockCLI.swift
//  BridgeBlockCLI
//
//  Created by Andrew Vanderbilt on 11/13/22.
//
// This is BridgeBlock.
// It monitors for Ethernet interfaces and disables wireless when wired is availible.
// Wireless interfaces almost always have weaker security, so this mitigates that risk.
// BridgeBlock, by nature of how it works, will break AirDrop and AirPlay, and any other wireless dependent features.
// This is not permanent however, just unplug the Ethernet interface and it will re-enable Wi-Fi.

import Foundation
import ArgumentParser

// These are global values which the user cannot change.

// Path the the Plist we scrape for network data.
let networkInterfacesPlist = URL(string: "file:///Library/Preferences/SystemConfiguration/NetworkInterfaces.plist")!

// Path to the temp file we use to preprocess the plist.
let fixedPlistPath = URL(string: "file:///var/tmp/fixed_plist.plist")!

// Store whether we have Internet access.
// We default to no, as this will re-enable Wi-Fi. Users won't like being kicked offline should something fail.
var hasInternet = false

@main
struct BridgeBlock: ParsableCommand {
    @Flag(help: "BridgeBlock will daemonize itself and run periodically in the background.")
    var daemonize = false
    
    @Flag(help: "Display BridgeBlock's software license info.")
    var license = false
    
    @Flag(help: "Run the program, but do not toggle WiFi. Useful for testing purposes.")
    var dry = false
    
    // Have to fix how we handle this. No user is going to prepend file:// to their path string.
    @Option(name: .shortAndLong, help: "Absolute path (including filename) of where to write the log file.")
    var logPath: String = "/var/tmp/anthurium.log"
    
    @Option(name: .shortAndLong, help: "Number of seconds to wait after the program starts before doing anything. Useful for daemon mode. Default is 0.")
    var startDelay: Int = 0
    
    @Option(name: .shortAndLong, help: "Use this URL to check if an Ethernet interface has Internet. Default is '8.8.8.8', AKA Google DNS.")
    var host: String = "8.8.8.8"
    
    @Option(name: .shortAndLong, help: "Number of ICMP echo packets that will be sent to determine connectivity. Default is 2.")
    var retry: Int = 2
    
    @Option(name: .shortAndLong, help: "How long in seconds before a ping attempt is abandoned. High latency networks may need to adjust this. Default is 1.")
    var timeout: Int = 1
    
    // This has no effect if daemon mode is not active.
    @Option(name: .shortAndLong, help: "When in daemon mode, wait this many seconds between interface checks. Default is 10.")
    var checkDelay: Int = 10
    
    mutating func run() throws {
        
        // Exit argumentparser, enter program.
        
        // Prepend the NSURL schema to the file path.
        logPath = "file://" + logPath
        
        // Accepts a file URL and returns its size in KB.
        func getFileSize(path: String) -> Int {
            do {
                let resourceValues = try URL(string: path)!.resourceValues(forKeys: [.fileSizeKey])
                let fileSizeKey = resourceValues.fileSize!
                return(fileSizeKey)
            } catch {
                // If we can't check the log file, we probably can't write to it either.
                // Print directly to console.
                print("ERROR: Could not check log file size.")
                print(error)
                return(0)
            }
        }
        
        // To prevent the log file from getting too big we'll be trimming it when it exceeds a hardcoded size.
        func trimLogFile(path: String) {
            
            print("INFO: Log file has exceeded maximum size. Deleting oldest 50% of log.")
            
            do {
                // Load file as a string.
                var logData = try String(contentsOf: URL(string: path)!)
                // Count the length, divide by two.
                let halfSize = logData.count / 2
                // Set index to half of log length.
                let index = logData.index(logData.startIndex, offsetBy: halfSize)
                // We'll use a tilde as the seperator char.
                logData.insert("~", at: index)
                // Split it.
                let splitLogData = logData.split(separator: "~")
                print(splitLogData[1])
                // Clear the log file
                // Write the half data splitLogData[1] back.
                try FileManager.default.removeItem(at: URL(string: path)!)
                try splitLogData[1].write(to: URL(string: path)!, atomically: true, encoding: .utf8)
                
            } catch {
                print("ERROR: Could not trim log file.")
                print(error)
            }
        }
        
        // Writes Strings to the log file, and prints them to console.
        // No, you cannot use appendToLog here, it creates recursion.
        func appendToLog(entry: String) {
            
            // Output the message to console.
            print("\(entry)")
            
            if FileManager.default.fileExists(atPath: URL(string: logPath)!.path) {
                
                // Check the log size. If too large call trimLogFile().
                if getFileSize(path: logPath) > 50000 {
                        trimLogFile(path: logPath)
                }
                // Append the entry to the end of the file.
                do {
                        let fileHandle = try FileHandle(forWritingTo: URL(string: logPath)!)
                        let data = entry.data(using: .utf8)
                        
                        try fileHandle.seekToEnd()
                        try autoreleasepool {
                            fileHandle.write(data!)
                            try fileHandle.close()
                        }
                    
                } catch {
                    print("ERROR: Could not append to log file.")
                }
            } else {
                // Create the file since it does not already exist.
                do {
                        try entry.write(to: URL(string: logPath)!, atomically: true, encoding: .utf8)
                } catch {
                    print("ERROR: Could not create log file. Program will continue without logging.")
                    print(error)
                }
            }
        }
        
        // Set up struct to convert Plist values to native objects.
        // https://stackoverflow.com/questions/60803515/swift-5-how-to-read-variables-in-plist-files
        struct Root: Decodable {
            let Interfaces: [Item]
        }
        
        struct SCNetworkInterfaceInfo: Decodable {
            let UserDefinedName: String
            // Not all interfaces will have these attributes.
            let idProduct: Int?
            let idVendor: Int?
            let kUSBProductString: String?
        }
        
        // Weird bug here. The Plist Decodable method can't handle whitespace in key strings.
        // For some reason, Apple uses a space in the BSD Name key.
        // To fix this, we proprocess the file before it reaches the decoder.
        // https://stackoverflow.com/questions/73884814/no-value-associated-with-key-codingkeysstringvalue-index-intvalue-nil
        struct Item: Decodable {
            
            // It seems that instead of setting this to false, they just won't include in the dict.
            let Active: Bool?
            let BSDName: String
            let IOBuiltin: Bool
            let IOInterfaceNamePrefix: String
            let IOInterfaceType: Int
            let IOInterfaceUnit: Int
            let IOMACAddress: Data
            let IOPathMatch: String
            let SCNetworkInterfaceInfo: SCNetworkInterfaceInfo
            let SCNetworkInterfaceType: String
        }
        
        // Pulls the list of available interfaces, returns nil if none found.
        // We'll handle the return of nil in the code that calls this function.
        func getInterfaces() -> [Item]? {
            
            do {
                
                // Read the copied file as a string.
                let brokenPlistData = try String(contentsOf: networkInterfacesPlist)
                
                // Remove the stupid space from the property key.
                let fixedPlistData = brokenPlistData.replacingOccurrences(of: "BSD Name", with: "BSDName")
                // .replacingOccurrences(of: "my sanity", with: "wtf apple?")
                
                // Write the data into a temporary file.
                try fixedPlistData.write(to: fixedPlistPath, atomically: true, encoding: .utf8)
                
                // Ingest the file using Data.
                let plistData = try Data(contentsOf: fixedPlistPath)
                
                // Decode the contents.
                var interfaces = [Item]()
                let result = try PropertyListDecoder().decode(Root.self, from: plistData)
                
                // Conform to Interfaces type.
                interfaces = result.Interfaces
                
                // Delete temporary file.
                try FileManager.default.removeItem(at: fixedPlistPath)
                
                return(interfaces)
                
            } catch {
                print(error)
            }
            // If we get an empty [Item], return nil.
            // We'll handle the return of nil in the code that calls this function.
            return(nil)
        }
        
        // Currently, we let the system guess which interface is wifi and to turn off.
        // TODO: Make an array of BSD names which are of 80211 interface type, and specifically set their power to off.
        @discardableResult // Make compiler shhhhh.
        func setAirport(state: String) -> Bool {
            
            let task = Process()
            let pipe = Pipe()
            
            task.standardOutput = pipe
            task.standardError = pipe
            // See the TODO above.
            // WIFI is not a valid interface name, so the system will assume what we want.
            task.arguments = ["-setairportpower", "WIFI", state]
            task.executableURL = URL(fileURLWithPath: "/usr/sbin/networksetup")
            task.standardInput = nil
            
            do {
                try task.run()
            } catch {
                appendToLog(entry: "ERROR: Execution of networksetup command failed!")
                appendToLog(entry: "ERROR: Unable to toggle 802.11 interfaces off.")
                appendToLog(entry: "\(error)")
                return(false)
            }
            
            return(true)
        }
        
        func canPing(host: String, interface: String) -> Bool {
            
            let task = Process()
            let pipe = Pipe()
            
            task.standardOutput = pipe
            task.standardError = pipe
            // Time out after 1 second.
            // Ping usually only takes 20-200 ms, so if we are exceeding 1000 something is wrong.
            task.arguments = ["-c", String(retry), "-t", String(timeout), "-b", interface, host]
            task.executableURL = URL(fileURLWithPath: "/sbin/ping")
            task.standardInput = nil
            
            do {
                try task.run()
            } catch {
                appendToLog(entry: "ERROR: Execution of ping command failed!")
                appendToLog(entry: "\(error)")
                return false
            }
            
            let data = pipe.fileHandleForReading.readDataToEndOfFile()
            let output = String(data: data, encoding: .utf8)!
            
            if output.contains("ping: sendto: No route to host") {
                // The interface (probably) does not have a working internet connection.
                appendToLog(entry: "INFO: ping found no route to host.")
                return(false)
            } else if output.contains("round-trip min/avg/max/stddev") {
                // This only appears in the results if the ping was successful at least once.
                appendToLog(entry: "INFO: ping reports interface has an Internet connection.")
                return(true)
            } else {
                return false
            }
        }
        
        // Here is where we need to insert the conditional code.
        
        // Fix user-supplied log paths so they conform to NSURL.
        if logPath == "file:///var/tmp/anthurium.log" {
            // User did not specify a log path.
        } else {
            // Whatever they supplied has to have the magic string prepended.
            logPath = "file://" + logPath
        }
        
        // If the user asked for license info, print it then terminate.
        if license == true {
            print("This software is provided under the GPLv2 license. A copy of the license can be found in the repository you obtained this software from.")
            abort()
        }
        
        
        // We use autoreleasepools because the Swift APIs are built atop CoreFoundation Obj-C types.
        // They leak is the short and sweet of it. Specifying an auto release pool allows the runtime to free memory after use.
        if daemonize == true {
            // Execute the main process on repeat.
            while true {
                   autoreleasepool {
                main()
                    }
                sleep(UInt32(checkDelay))
            }
            
        } else {
            // Execute main() once.
             autoreleasepool {
            main()
              }
        }
        
        // Main program loop.
        
        func main () {
            
            
            // Time to wait before we do anything.
            sleep(UInt32(startDelay))
            
            // Fetch the interfaces.
            let interfaces = getInterfaces()
            
            // If there are no items in the interfaces array, we handle it here.
            // If none are found the function returns nil.
            for item in interfaces ?? [] {
                
                // Have to handle the possibility of there being no interfaces.
                // Realistically, this should never happen due to the SE coprocessor bridges, but still.
                if interfaces == nil {
                    appendToLog(entry: "WARNING: No interfaces detected. Are you sure this is correct?")
                    abort()
                }
                
                // We don't want to count iBridges or iOS devices. These interfaces are used for coprocessors or shared hotspots.
                // The interfaces we want must also be listed as Active.
                if item.SCNetworkInterfaceType == "Ethernet" && item.SCNetworkInterfaceInfo.UserDefinedName != "iBridge" &&
                    item.SCNetworkInterfaceInfo.UserDefinedName != "iPad" && item.SCNetworkInterfaceInfo.UserDefinedName != "iPhone" && item.Active == true {
                    
                    appendToLog(entry: " Found Ethernet interface: \(item)\n\n")
                    
                    // Send out a ping using the interface.
                    if canPing(host: host, interface: item.BSDName) {
                        hasInternet = true
                        appendToLog(entry: "INFO: Host \(host) was pinged sucessfully. Interface has an Internet connection")
                        if dry == false {
                            setAirport(state: "off")
                        }
                        
                        // No need to test the rest, we have fufilled our purpose.
                        break
                        
                    } else {
                        hasInternet = false
                        appendToLog(entry: "INFO: Host \(host) unreachable. Interface may not have an Internet connection")
                    }
                }
                
            }
            // The connectivity test failed. Turn Airport back on.
            if hasInternet == false && dry == false {
                    setAirport(state: "on")
            }
        }
    }
}
